#/bin/bash -x

BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
docker build -t "registry.gitlab.com/registry2092427/composer-php:${PHP_VERSION}-mysql" -f ./mysql/Dockerfile \
    --build-arg "BUILD_DATE=${BUILD_DATE}" \
    --build-arg "PHP_VERSION=${PHP_VERSION}" \
    --build-arg "COMPOSER_VERSION=2"
docker push "registry.gitlab.com/registry2092427/composer-php:${PHP_VERSION}-mysql"
